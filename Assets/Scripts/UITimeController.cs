using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UITimeController : MonoBehaviour
{
    public int min, seg;
    public Text time;
    public int sceneNumber;

    private bool _isRunningTimer;
    private float _res;
    // Start is called before the first frame update
    void Start()
    {
        _res = (min * 60) + seg;
        _isRunningTimer = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (_isRunningTimer)
        {
            _res -= Time.deltaTime;
            if (_res < 1)
            {
                _isRunningTimer=false;
                SceneManager.LoadScene(sceneNumber);
            }
            int tempMin = Mathf.FloorToInt(_res / 60);
            int tempSeg = Mathf.FloorToInt(_res % 60);
            time.text = string.Format("{00:00}:{01:00}", tempMin, tempSeg);
        }
    }
}

using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerCollideWithFireBall : MonoBehaviour
{
    private Animator _animator;
    private int _sceneID;
    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
        _sceneID = SceneManager.GetActiveScene().buildIndex;
        Debug.Log(_sceneID);
    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("FireBall"))
        {
            if (_sceneID==1)
            {
                SceneManager.LoadScene(2);
            }
            else
            {
                SceneManager.LoadScene(3);
            }
            
        }
        
    }
}

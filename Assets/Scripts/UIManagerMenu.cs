using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManagerMenu : MonoBehaviour
{
    public Slider sliderChooseGame;
    public InputField inputFieldName;
    public Text stringName;
    public void ChangeScene()
    {
        stringName.text = inputFieldName.GetComponent<InputField>().text;
        Debug.Log(stringName.text);
        float getSlideChoose = sliderChooseGame.value;
        Debug.Log(getSlideChoose);
        
        if (getSlideChoose == 0 && stringName.text.Length > 2)
        {
            SceneManager.LoadScene(1);
        }
        if (getSlideChoose == 1 && stringName.text.Length > 2)
        {
            SceneManager.LoadScene(4);
        }
    }
}

using UnityEngine;
public enum HabilityGiant
{
    Augment,
    Decrease,
    GiantTime,
    Cooldown,
    Nothing
}

public class GiantHability : MonoBehaviour
{
    public HabilityGiant hG;
    private float _timerBig;
    public float BigDuration = 5f;
    private float _bigAtenuator = 2f;
    private float _velocityBig;
    private float _originalSize;
    // Start is called before the first frame update
    void Start()
    {
        _originalSize = transform.localScale.x;
        hG = HabilityGiant.Nothing;
    }

    // Update is called once per frame
    void Update()
    {
        switch (hG)
        {
            case HabilityGiant.Augment:
                BigHabilty();
                break;
            case HabilityGiant.Decrease:
                DecHab();
                break;
            case HabilityGiant.GiantTime:
                TimerBig();
                break;
            case HabilityGiant.Cooldown:
                Cooldown();
                break;
        }
    }
    private void BigHabilty()
    {
        _velocityBig = _bigAtenuator * Time.deltaTime;
        if (transform.localScale.x < _originalSize * 3)
        {
            transform.localScale += new Vector3(_velocityBig, _velocityBig, 0);
        }
        else
        {
            hG = HabilityGiant.GiantTime;
        }
    }

    private void DecHab()
    {
        _velocityBig = _bigAtenuator * Time.deltaTime;
        if (_originalSize < transform.localScale.x)
        {
            transform.localScale -= new Vector3(_velocityBig, _velocityBig, 0);
        }
        else
        {
            hG = HabilityGiant.Cooldown;
        }
    }
    public void TimerBig()
    {
        if (_timerBig < BigDuration)
        {
            _timerBig += Time.deltaTime;
        }
        else
        {
            hG = HabilityGiant.Decrease;
            _timerBig = 0;
        }
    }
    public void Cooldown()
    {
        if (_timerBig < 10f)
        {
            _timerBig += Time.deltaTime;
        }
        else
        {
            _timerBig = 0;
            hG = HabilityGiant.Nothing;
        }
    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Mushroom"))
        {
            hG = HabilityGiant.Augment;
            Destroy(other.gameObject);
        }
    }
}

using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    public float speed = 0.01f;
    private Animator _animator;
    private Rigidbody2D _rb;
    private SpriteRenderer _sprite;

    private float _horizontal;
    public float JumpForce;
    private bool _isJump = false;
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
        _sprite = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        _animator.SetBool("Jump", _isJump);
        Debug.Log(_isJump);
        _horizontal = Input.GetAxis("Horizontal");
        _animator.SetBool("Running", _horizontal != 0.0f);
        transform.position = new Vector3 (speed * _horizontal + transform.position.x, transform.position.y, transform.position.z);
        //Debug.DrawLine(transform.position, Vector3.down*0.1f, Color.red);
        
        if (Input.GetKeyDown(KeyCode.Space) && !_isJump)
        {           
            Jump();
        }
        if (_horizontal < 0.0f) _sprite.flipX = true;  
        else if (_horizontal > 0.0f) _sprite.flipX = false;

        if (transform.position.y <= -5f)
        {
            SceneManager.LoadScene(2);
        }
    }
    private void Jump()
    {
        _isJump = true;
        //_animator.SetBool("Jump", _isJump);
        _rb.AddForce(Vector3.up * JumpForce);
    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        //Si el jugador colisiona con un objeto con la etiqueta suelo
        if (other.gameObject.CompareTag("Ground"))
        {
            
            //Digo que no est� saltando (para que pueda volver a saltar)
            _isJump = false;
            //Le quito la fuerza de salto remanente que tuviera
           _rb.velocity = new Vector2(_rb.velocity.x, 0);
        }
    }
    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Ground"))
        {
            _isJump = true;            
        }
    }
}

using UnityEngine;

public class MushroomCollide : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Ground"))
        {
            Destroy(gameObject, 10f);
        }
    }
}

using UnityEngine;

public class CollidesOnTheFloor : MonoBehaviour
{
    private Animator _animator;

    void Awake()
    {
        _animator = GetComponent<Animator>();
    }
    void Update()
    {
        if (transform.position.y <= -7f)
        {
            Destroy(GetComponent<Rigidbody2D>());
            Destroy(gameObject);
        } 
    }
    private void OnCollisionEnter2D(Collision2D other)
    {       
        if (other.gameObject.CompareTag("Ground"))
        {
            transform.position += new Vector3(0, -0.5f, 0);            
            Destroy(GetComponent<Rigidbody2D>());
            Destroy(GetComponent<Collider2D>());
            _animator.SetBool("Explosion", true);
            Destroy(gameObject, 2.3f);
            
        }
    }
}

using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManagerGame : MonoBehaviour
{
    private GameObject getNamePlayer;
    public Text namePlayer;
    // Start is called before the first frame update
    void Start()
    {
        PlayerPrefs.SetInt("SceneNumber", SceneManager.GetActiveScene().buildIndex);
        getNamePlayer = GameObject.Find("GetNameSaveNextScreen");
       namePlayer.text = getNamePlayer.GetComponent<Text>().text;       
    }
}

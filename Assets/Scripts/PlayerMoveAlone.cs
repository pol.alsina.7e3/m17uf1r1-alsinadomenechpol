using UnityEngine;

public class PlayerMoveAlone : MonoBehaviour
{

    public float speed;
    [SerializeField] private float _rightBoundary;
    [SerializeField] private float _leftBoundary;
    private float dirX = -1;
    RaycastHit2D hit;


    public float JumpForce;
    private Animator _animator;
    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();       
    }

    // Update is called once per frame
    void Update()
    {
        
        
        PlayerBoundaries();
        _animator.SetBool("Running", dirX * speed + transform.position.x != 0.0f);       
        transform.position = new Vector3(dirX * speed + transform.position.x, transform.position.y, transform.position.z);
        ChangeDirection();
        FlipSprite();        
        RaycastHitFireBall();

    }
    private void FlipSprite()
    {
        if (dirX == -1f)
        {
            this.GetComponent<SpriteRenderer>().flipX = true;
        }
        else
        {

            this.GetComponent<SpriteRenderer>().flipX = false;
        }
    }
    private void PlayerBoundaries()
    {
        if (transform.position.x >= _rightBoundary)
        {
            transform.position = new Vector3(_rightBoundary, transform.position.y, 0);
        }
        else if (transform.position.x <= _leftBoundary)
        {
            transform.position = new Vector3(_leftBoundary, transform.position.y, 0);
        }
    }
    public void ChangeDirection()
    {   
        if (gameObject.transform.position.x <= _leftBoundary)
        {
            dirX = 1f;
        }
        else if (gameObject.transform.position.x >= _rightBoundary)
        {
            dirX = -1f;
        }        
    }
    public void RaycastHitFireBall()
    {
        //Debug.DrawLine(transform.position, new Vector2(transform.position.x, transform.position.y+4), Color.red);
        hit = Physics2D.Raycast(transform.position, Vector2.up);
        if (hit.collider != null)
        {
            if (hit.collider.tag == "FireBall")
            {
                if (dirX == 1)
                {
                    dirX = -1;
                }
                else
                {
                    dirX = 1;
                }
                Debug.Log("aaa");
            }
            
        }
    }
}

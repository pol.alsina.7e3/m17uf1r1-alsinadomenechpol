using UnityEngine;

public class SpawnClickFireBall : MonoBehaviour
{
    public float limitsY;
    public GameObject fireball;

    private float _time;
    private float _interval = 0.5f;
    void Update()
    {
        _time += Time.deltaTime;
        if (Input.GetMouseButtonDown(0))
        {
            if (_time > _interval)
            {
                Vector3 worldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                Instantiate(fireball, new Vector3(worldPosition.x, limitsY, 0), Quaternion.identity);
                fireball.SetActive(true);
                _time = 0;
            }
        }
    }
}

using UnityEngine;
using UnityEngine.SceneManagement;

public class VictoryUI : MonoBehaviour
{
    public Animator animator;
    private int _numberSceneToReturn;
    void Awake()
    {
        _numberSceneToReturn = PlayerPrefs.GetInt("SceneNumber");
    }
    void Start()
    {       
        if (_numberSceneToReturn >= 4) animator.SetBool("Death", true);
    }
    public void Game1SceneCHange()
    {
        SceneManager.LoadScene(_numberSceneToReturn);
    }

    public void MenuSceneCHange()
    {
        SceneManager.LoadScene(0);       
    }
}

using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManagerGameOver : MonoBehaviour
{
    public Animator animator;
    private int _numberSceneToReturn;
     void Awake()
    {
        _numberSceneToReturn = PlayerPrefs.GetInt("SceneNumber");
    }
    void Start()
    {
        Debug.Log(_numberSceneToReturn);
        if (_numberSceneToReturn < 4) animator.SetBool("Death", true);        
    }
    public void ChangeSceneMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void ChangeSceneGame()
    {
        SceneManager.LoadScene(_numberSceneToReturn);
    }
}
